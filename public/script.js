$.material.init();

var json = {
    title: "Individual Activity: Are You Productive?",
    showProgressBar: "top",
    pages: [{
        questions: [{
            type: "matrix",
            name: "header",
            title: "Please select 1 for 'rarely', 2 for 'sometimes' and 3 for 'often'to indicate how consistently you use the described behavior in the workplace.",
            columns: [{
                value: 1,
                text: "1"
            }, {
                value: 2,
                text: "2"
            }, {
                value: 3,
                text: "3"
            },],
            rows: [{
                value: "spend",
                text: "I spend 10–15 minutes planning my day."
            }, {
                value: "check",
                text: "I check every email as soon as it arrives."
            },{
                value: "day",
                text: "My day is organized by the key results I need to achieve."
            },{
                value: "mess",
                text: "My desk and files are a mess."
           }, {
                value: "leave",
                text: "I leave open pockets of time in my schedule."
            }, {
                value: "late",
                text: " I’m habitually late."
            }, {
                value: "done",
                text: "I multi-task to try to get more done."
            }, {
                value: "breaks",
                text: "I take mini-breaks throughout the day."
            }, {
                value: "big",
                text: "I usually procrastinate on big projects."
            },{
                value: "seek",
                text: "I actively seek out projects that stretch my skills."
            },{
                value: "rut",
                text: "I feel I am in a rut at work."
            }]
        }]
        }]
   }     
Survey.defaultBootstrapMaterialCss.navigationButton = "btn btn-green";
Survey.defaultBootstrapMaterialCss.rating.item = "btn btn-default my-rating";
Survey.Survey.cssType = "bootstrapmaterial";

var survey = new Survey.Model(json);

survey.onComplete.add(function(result) {

	var sum = 0;
	var rows = Object.values(result.data);
	for(var i = 0; i < rows.length; i++){
		var row = Object.values( rows[i] );
		for(var j = 0; j < row.length; j++) {
			var val = row[j];
			sum += parseInt(val);
		}
	}


function myFunction(sum) 

{
    var scoring;
    if (sum <= 39 && sum >= 31) {
        scoring = "You know how to look for the positives! You’ve got a handle on how to provide feedback, and those who receive this feedback likely know as much.";
    } else if (sum <= 30 && sum >= 21) {
        scoring = "You have an awareness of what makes good feedback, but not all the time. Are there specific situations, environments, or types of people that make it harder for you to give proper feedback?";
    } else if (sum <= 20 && sum >= 13) {
        scoring = "You may not be as constructive a criticizer as you’d like to believe. Next time you’re giving feedback, ask yourself, would you like to hear feedback the way you’re administering it?";
    } else {
        scoring = "Error";
    }
    
document.querySelector('#result').innerHTML = "Your Score " + sum + "<br>" + "According to the survey, " + scoring;

} 

myFunction(sum);

});
       
survey.render("surveyElement");